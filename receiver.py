import sqlite3
from flask import Flask, request, session, g, redirect, url_for, \
     abort, render_template, flash

DATABASE = 'receiver.db'
DEBUG = True

app = Flask(__name__)
app.config.from_object(__name__)

def connect_db():
    return sqlite3.connect(app.config['DATABASE'])

@app.before_request
def before_request():
    g.db = connect_db()

@app.teardown_request
def teardown_request(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()

@app.route('/response/<user_id>/<machine_id>/<machine_action>')
def receive_response(user_id, machine_id, machine_action):
    print "%s %s %s" % (user_id, machine_id, machine_action)
    g.db.execute('insert into responses(user_id, machine_id, machine_action) values (?, ?, ?)', [user_id, machine_id, machine_action])
    g.db.commit()
    return render_template('response_received.html')

if __name__ == '__main__':
    app.run()
