create table responses (
  user_id text not null,
  machine_id text not null,
  machine_action not null,
  response_time datetime DEFAULT CURRENT_TIMESTAMP
);
